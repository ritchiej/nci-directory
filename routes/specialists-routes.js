
const conn = require('../db/connection')
const zipcodes = require('zipcodes')
const states = require('us-state-codes')

module.exports.specialists = async (req, res) => {
  let select = ` SELECT
    gp.firstname, gp.lastname, gp.degree, gp.profession_type,
    pl.institution, pl.address, pl.city, pl.state, pl.zip, pl.country, pl.phone, pl.email `
  if(req.query.syndrome){
    select += ', css.cancer '
  }
  if(req.query.cancer_type){
    select += ', ct.cancer_type '
  }
  select += ` FROM genetics_professional gp
  JOIN practice_locations pl ON pl.gp_id = gp.gp_id `

  let where = ' WHERE '
  let groupBy = ` GROUP BY
    gp.firstname, gp.lastname, gp.degree, gp.profession_type,
    pl.institution, pl.address, pl.city, pl.state, pl.zip, pl.country, pl.phone, pl.email,
    ct.cancer_type `
  let replacements = {}
  let count = 1;
  const queryLength = Object.keys(req.query).length
  for (const q in req.query) {
    if (q === 'zip') {
      where = where + ' pl.' + q + ' IN (:' + q + 's) '
      replacements.zips = zipcodes.radius(req.query.zip, 50)
    } else if (q === 'state' || q === 'city') {
      where = where + ' LOWER(pl.' + q + ') = LOWER(:' + q + ') '
      if (q === 'state') {
        replacements.state = (req.query[q].length === 2) ? req.query[q].toUpperCase() : states.getStateCodeByStateName(req.query[q])
      } else if (q === 'city') {
        replacements.city = req.query[q]
      }
    } else if (q === 'syndrome') {
      if(!req.query.cancer_type){
        select += ' JOIN cancer_syndrome_service css ON css.gp_id = gp.gp_id '
      }
        where = where + ' LOWER(css.cancer) = LOWER(:' + q + ') '
      replacements.syndrome = req.query[q]
      groupBy += ', css.cancer '
    } else if (q === 'cancer_type') {
      select += ' JOIN cancer_syndrome_service css ON css.gp_id = gp.gp_id '
      select += ' JOIN cancer_type ct ON css.css_id = ct.css_id '
      where = where + ' LOWER(ct.' + q + ') = LOWER(:' + q + ') '
      replacements.cancer_type = req.query[q]
    } else {
      console.err('Did not know what to do with query parameter ' + q + ': ' + req.query[q])
    }

    if (count < queryLength) {
      where += ' AND '
    }
    count++
  }

  let query = select + where
  if (req.query.type) {
    query += groupBy
  }

  const specialists = await conn.query(query, {
      replacements: replacements,
      type: conn.QueryTypes.SELECT
  })
  res.json(specialists)
}
