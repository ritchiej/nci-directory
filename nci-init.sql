

CREATE TABLE IF NOT EXISTS genetics_professional (
    gp_id SERIAL PRIMARY KEY,
    sname VARCHAR(255),
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    degree VARCHAR(255),
    profession_type VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS practice_locations (
    pl_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    institution VARCHAR(255),
    address VARCHAR(255),
    city VARCHAR(255),
    state VARCHAR(255),
    zip VARCHAR(255),
    country VARCHAR(255),
    phone VARCHAR(255),
    email VARCHAR(255),
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);

CREATE TABLE IF NOT EXISTS specialty (
    s_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    specialty VARCHAR(255),
    board_certified BOOLEAN,
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);

CREATE TABLE IF NOT EXISTS team_service (
    ts_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    service VARCHAR(255),
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);

CREATE TABLE IF NOT EXISTS cancer_syndrome_service (
    css_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    cancer VARCHAR(255),
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);

CREATE TABLE IF NOT EXISTS cancer_type (
    ct_id SERIAL PRIMARY KEY,
    css_id INTEGER NOT NULL,
    cancer_type VARCHAR(255),
    cancer_sites VARCHAR(255),
    FOREIGN KEY (css_id) REFERENCES cancer_syndrome_service(css_id)
);

CREATE TABLE IF NOT EXISTS membership (
    m_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    institution VARCHAR(1000),
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);
