
require('dotenv').config()
const fs = require('fs')
const path = require('path')
const util = require('util')
const chalk = require('chalk')
const debug = require('debug')('db-loader')
const parseString = require('xml2js').parseString
const tables = require('./model/tables')
const conn = require('./connection')

const readFile = util.promisify(fs.readFile)
const parseToJson = util.promisify(parseString);
const sql = path.join(__dirname, "..", "nci-init.sql")

loadDB()

async function loadDB() {
  conn.authenticate().then(() => {
    debug(chalk.green('Connection has been established successfully.'))
  })
  .catch(err => {
    console.error(chalk.red('Unable to connect to the database:'), err, conn.config)
  })

  await createDb()

  const cdrFilePath = path.join(__dirname, "..", "data", "GeneticsProfessional")
  const files = fs.readdirSync(cdrFilePath)

  for(const f of files) {
    console.log('FILE: ' + f)
    await loadCdr(f, cdrFilePath)
  }

  console.log("NCI Directory successfully loaded!")
  process.exit()
}

async function createDb() {
  console.log("Creating db")
  const init = fs.readFileSync(sql).toString()
  return conn.query(init)
}

async function loadCdr(fileName, cdrFilePath) {
  const file = await readFile(path.join(cdrFilePath, fileName))
  const xmlFile = file.toString()
  const cdr = await parseToJson(xmlFile);

  const gpId = await saveGeneticsProfessional(cdr)
  await savePracticeLocations(cdr, gpId)
  await saveSpecialties(cdr, gpId)
  await saveTeamServices(cdr, gpId)
  await saveCancerSyndromeServices(cdr, gpId)

  await saveMemberships(cdr, gpId)

}

async function saveGeneticsProfessional(cdr) {
  const geneticsProfessional = {
    sname: cdr.GENETICSPROFESSIONAL.NAME[0].SNAME.join(),
    firstname: cdr.GENETICSPROFESSIONAL.NAME[0].FIRSTNAME.join(),
    lastname: cdr.GENETICSPROFESSIONAL.NAME[0].LASTNAME.join(),
    degree: cdr.GENETICSPROFESSIONAL.DEGREE.join(),
    profession_type: cdr.GENETICSPROFESSIONAL.TYPE.join()
  }

  const gp = await tables.geneticsProfessional.create(geneticsProfessional)
  return gp.dataValues.gp_id
}

async function savePracticeLocations (cdr, gpId) {
  const practiceLocations = cdr.GENETICSPROFESSIONAL.PRACTICELOCATIONS
  for(const pl of practiceLocations) {
    practiceLocation = {
      gp_id: gpId,
      address: pl.CADD.join(' '),
      city: pl.CCIT.join(),
      country: pl.CCTY.join()
    }
    if(pl.INSTITUTION){
      practiceLocation.institution = pl.INSTITUTION.join()
    }
    if(pl.CPHN) {
      practiceLocation.phone = pl.CPHN.join()
    }
    if(pl.CEML) {
      practiceLocation.email = pl.CEML.join()
    }
    if(pl.CPUN) {
      practiceLocation.state = pl.CPUN.join()
    }
    if(pl.CCOD) {
      practiceLocation.zip = pl.CCOD.join()
    }
    await tables.practiceLocations.create(practiceLocation)
  }
}

async function saveSpecialties (cdr, gpId) {
  const specialties = cdr.GENETICSPROFESSIONAL.SPECIALTY
  if(specialties){
    for(const s of specialties) {
      specialty = {
        gp_id: gpId,
        specialty: s.SPECIALTYNAME.join(),
        board_certified: (s.BDCT.join() === 'Yes') ? true : false
      }
      await tables.specialty.create(specialty)
    }
  }
}

async function saveTeamServices (cdr, gpId) {
  const teamServices = cdr.GENETICSPROFESSIONAL.TEAMSERVICES
  if(teamServices) {
    for(const t of teamServices) {
      teamService = {
        gp_id: gpId,
        service: t
      }
      await tables.teamService.create(teamService)
    }
  }
}

async function saveCancerSyndromeServices (cdr, gpId) {
  const geneticServices = cdr.GENETICSPROFESSIONAL.GENETICSERVICES
  for(const g of geneticServices) {
    for(const f of g.FAMILYCANCERSYNDROME) {
      cancerSyndromeService = {
        gp_id: gpId,
        cancer: f.SYNDROMENAME.join()
      }
      const cssId = await tables.cancerSyndromeService.create(cancerSyndromeService)
      await saveCancerTypes(f, cssId.dataValues.css_id)
    }
  }
}

async function saveCancerTypes (f, cssId) {
  for(const t of f.CANCERTYPE) {
    cancerType = {
      css_id: cssId,
      cancer_type: t.TYPENAME.join(),
      cancer_sites: t.CANCERSITE.join()
    }
    await tables.cancerType.create(cancerType)
  }
}

async function saveMemberships (cdr, gpId) {
  const memberships = cdr.GENETICSPROFESSIONAL.MEMBERSHIP
  if(memberships){
    for(const m of memberships) {
      membership = {
        gp_id: gpId,
        institution: m.INSTITUTION.join()
      }
      await tables.membership.create(membership)
    }
  }
}
