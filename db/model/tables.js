
const conn = require('../connection')
const Sequelize = require('sequelize')

const geneticsProfessional = conn.define('genetics_professional', {
  gp_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  sname: { type: Sequelize.STRING },
  firstname: { type: Sequelize.STRING },
  lastname: { type: Sequelize.STRING },
  degree: { type: Sequelize.STRING },
  profession_type: { type: Sequelize.STRING }
}, {
  timestamps: false,
  freezeTableName: true
})

const practiceLocations = conn.define('practice_locations', {
  pl_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  gp_id: {
    type: Sequelize.INTEGER,
    references: {
      model: geneticsProfessional,
      key: 'gp_id',
      deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
  },
  institution: { type: Sequelize.STRING },
  address: { type: Sequelize.STRING },
  city: { type: Sequelize.STRING },
  state: { type: Sequelize.STRING },
  zip: { type: Sequelize.STRING },
  country: { type: Sequelize.STRING },
  phone: { type: Sequelize.STRING },
  email: { type: Sequelize.STRING }
}, {
  timestamps: false,
  freezeTableName: true
})

const specialty = conn.define('specialty', {
  s_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  gp_id: {
    type: Sequelize.INTEGER,
    references: {
      model: geneticsProfessional,
      key: 'gp_id',
      deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
  },
  specialty: { type: Sequelize.STRING },
  board_certified: { type: Sequelize.BOOLEAN }
}, {
  timestamps: false,
  freezeTableName: true
})

const teamService = conn.define('team_service', {
  ts_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  gp_id: {
    type: Sequelize.INTEGER,
    references: {
      model: geneticsProfessional,
      key: 'gp_id',
      deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
  },
  service: { type: Sequelize.STRING }
}, {
  timestamps: false,
  freezeTableName: true
})

const cancerSyndromeService = conn.define('cancer_syndrome_service', {
  css_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  gp_id: {
    type: Sequelize.INTEGER,
    references: {
      model: geneticsProfessional,
      key: 'gp_id',
      deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
  },
  cancer: { type: Sequelize.STRING },
}, {
  timestamps: false,
  freezeTableName: true
})

const cancerType = conn.define('cancer_type', {
  ct_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  css_id: {
    type: Sequelize.INTEGER,
    references: {
      model: cancerSyndromeService,
      key: 'css_id',
      deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
  },
  cancer_type: { type: Sequelize.STRING },
  cancer_sites: { type: Sequelize.STRING }
}, {
  timestamps: false,
  freezeTableName: true
})

const membership = conn.define('membership', {
  m_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  gp_id: {
    type: Sequelize.INTEGER,
    references: {
      model: geneticsProfessional,
      key: 'gp_id',
      deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
  },
  institution: { type: Sequelize.STRING }
}, {
  timestamps: false,
  freezeTableName: true
})

module.exports = {
  geneticsProfessional,
  practiceLocations,
  specialty,
  teamService,
  cancerSyndromeService,
  cancerType,
  // cancerSite,
  membership
}
